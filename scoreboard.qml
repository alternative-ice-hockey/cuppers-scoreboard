import QtQuick 1.0
import "content"
import "content/logic.js" as Logic

Rectangle {
	id: screen
	width: 1024; height: 600
	SystemPalette { id: activePalette }

	Image {
		id: logo
		anchors { right: parent.right; bottom: parent.bottom }
		fillMode: Image.PreserveAspectFit
		smooth: true
		source: "alts-logo.svg"
		width: 500
		opacity: 0.3
	}

	ParallaxView {
		id: parallaxView
		anchors.fill: parent
		focus: true

		GroupStagesPanel {
			id: groupStagesPanel
			width: screen.width
			height: screen.height
			focus: true
		}

		FocusScope {
			id: groupPlayoffsPanel
			width: screen.width
			height: screen.height
			visible: Logic.hasGroupStagePlayoffs()
			Text {
				id: gpoTitle
				anchors.horizontalCenter: parent.horizontalCenter
				text: "Group Stage Playoffs"
				font.bold: true
				font.pixelSize: 20
			}
			GameView {
				id: groupPlayoffsGameView
				anchors {
					horizontalCenter: parent.horizontalCenter
					top: gpoTitle.bottom
					topMargin: 5
					bottom: parent.bottom
					bottomMargin: 10
				}
				focus: true
			}
		}

		FocusScope {
			id: quarterFinalsPanel
			width: screen.width
			height: screen.height
			Text {
				id: qfTitle
				anchors.horizontalCenter: parent.horizontalCenter
				text: "Quarter Finals"
				font.bold: true
				font.pixelSize: 20
			}
			GameView {
				id: quarterFinalsGameView
				anchors {
					horizontalCenter: parent.horizontalCenter
					top: qfTitle.bottom
					topMargin: 5
					bottom: parent.bottom
					bottomMargin: 10
				}
				focus: true
			}
		}

		FocusScope {
			id: semiFinalsPanel
			width: screen.width
			height: screen.height
			Text {
				id: sfTitle
				anchors.horizontalCenter: parent.horizontalCenter
				text: "Semi Finals"
				font.bold: true
				font.pixelSize: 20
			}
			GameView {
				id: semiFinalsGameView
				anchors {
					horizontalCenter: parent.horizontalCenter
					top: sfTitle.bottom
					topMargin: 5
					bottom: parent.bottom
					bottomMargin: 10
				}
				focus: true
			}
		}

		Loader {
			id: finalPanelLoader
			width: screen.width
			height: screen.height
		}

		onCurrentIndexChanged: {
			switch (currentIndex) {
				case 0:
					groupStagesPanel.focus = true;
				case 1:
					if (Logic.generateGroupStagePlayoffGames()) {
						groupPlayoffsGameView.model = Logic.group_stage_playoffs;
						groupPlayoffsPanel.focus = true;
						//groupPlayoffsGameView.focus = true;
					}
					break;
				case 2:
					if (Logic.generateQuarterFinalGames()) {
						quarterFinalsGameView.model = Logic.quarter_final_games;
						quarterFinalsPanel.focus = true;
						//quarterFinalsGameView.focus = true;
					} else if (Logic.hasGroupStagePlayoffs()) {
						--currentIndex;
					}
					break;
				case 3:
					if (Logic.generateSemiFinalGames()) {
						semiFinalsGameView.model = Logic.semi_final_games;
						semiFinalsGameView.focus = true;
					} else {
						--currentIndex;
					}
					break;
				case 4:
					if (Logic.generateFinalGames()) {
						finalPanelLoader.source = "content/FinalAndPlayoffPanel.qml";
					} else {
						--currentIndex;
					}
					break;
			}
		}
	}

	ImageButton {
		id: nextButton
		anchors {
			right: parent.right
			rightMargin: 10
			bottom: parent.bottom
			bottomMargin: 10
		}
		source: "content/images/next.svg"
		width: 25; height: 25
		sourceSize.width: 25
		sourceSize.height: 25
		visible: parallaxView.currentIndex < 4
		onClicked: {
			if (!Logic.hasGroupStagePlayoffs() && parallaxView.currentIndex == 0)
				parallaxView.currentIndex = 2;
			else
				++parallaxView.currentIndex;
		}
	}

	ImageButton {
		id: previousButton
		anchors {
			left: parent.left
			leftMargin: 10
			bottom: parent.bottom
			bottomMargin: 10
		}
		source: "content/images/previous.svg"
		width: 25; height: 25
		sourceSize.width: 25
		sourceSize.height: 25
		visible: parallaxView.currentIndex > 0
		onClicked: {
			if (!Logic.hasGroupStagePlayoffs() && parallaxView.currentIndex == 2)
				parallaxView.currentIndex = 0;
			else
				--parallaxView.currentIndex;
		}
	}
}

// vim:ft=qml
