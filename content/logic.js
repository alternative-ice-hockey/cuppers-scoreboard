.pragma library

Qt.include("../scoreboard-logic.js");

function groupColor(group) {
	switch (group) {
		case 0:
			return "powderBlue";
		case 1:
			return "paleGreen";
		case 2:
			return "lightPink";
		case 3:
			return "thistle";
		default:
			return "peachPuff";
	}
}

function limitText(text, length) {
	if (text.length > length) {
		return text.slice(0, length-3) + '...';
	} else {
		return text;
	}
}

function darkGroupColor(group) {
	switch (group) {
		case 0:
			return "royalBlue";
		case 1:
			return "lawnGreen";
		case 2:
			return "hotPink";
		case 3:
			return "mediumpurple";
		default:
			return "salmon";
	}
}

function gameColor(game, isGroupGame, deep) {
	if (isGroupGame) {
		if (game) {
			if (deep)
				return darkGroupColor(game.team1.group);
			else
				return groupColor(game.team1.group);
		} else {
			return "gray";
		}
	}
	return deep ? "mediumpurple" : "thistle";
}
