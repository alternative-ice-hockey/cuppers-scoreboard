import QtQuick 1.0
import "logic.js" as Logic

ListView {
	width: 800
	clip: true
	delegate: Item {
		width: parent.width; height: contents.height + 4
		Item {
			width: parent.width;
			height: childrenRect.height;
			id: contents
			property variant games: model.modelData
			Repeater {
				model: (contents.games[1]) ? 2 : 1
				GamePanel {
					y: 2
					anchors {
						left: (index == 0) ? parent.left : parent.horizontalCenter
						leftMargin: (index == 0) ? 0 : 5
						right: (index == 0) ? parent.horizontalCenter : parent.right
						rightMargin: (index == 0) ? 5 : 0
					}
					game: contents.games[index]
					isGroupGame: true
					playButtonEnabled: true
					onPlayClicked: {
						Logic.startHalfIceGames(contents.games[0], contents.games[1]);
					}
					onStopClicked: {
						Logic.endCurrentGames();
					}
				}
			}
		}
	}
}

// vim:ft=qml
