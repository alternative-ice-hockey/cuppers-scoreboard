import QtQuick 1.0

Row {
	id: container

	property bool inEditMode: false
	property alias teamName: teamNameText.text
	property int teamScore: 0
	property bool showScore: false
	property bool isWinner: false

	height: Math.max(teamNameText.height, teamScoreText.height)
	spacing: 5
	ImageButton {
		id: minusButton
		source: "images/minus.svg"
		width: parent.height; height: parent.height
		sourceSize.width: parent.height
		sourceSize.height: parent.height
		visible: inEditMode
		onClicked: { if (teamScore > 0) --teamScore }
	}
	Text {
		id: teamNameText
		font.bold: container.isWinner
	}
	Text {
		id: teamScoreText
		text: "(" + container.teamScore + ")"
		visible: showScore || inEditMode
	}
	ImageButton {
		id: plusButton
		source: "images/plus.svg"
		width: parent.height; height: parent.height
		sourceSize.width: parent.height
		sourceSize.height: parent.height
		visible: inEditMode
		onClicked: { ++teamScore }
	}
	Behavior on inEditMode {
		SequentialAnimation {
			NumberAnimation { target: container; property: "opacity"; to: 0; duration: 150 }
			PropertyAction {}
			NumberAnimation { target: container; property: "opacity"; to: 1; duration: 150 }
		}
	}
}

// vim:ft=qml
