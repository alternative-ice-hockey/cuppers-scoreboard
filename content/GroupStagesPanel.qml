import QtQuick 1.0
import "logic.js" as Logic

Item {
	id: container

	GroupLists {
		id: groupLists
		//anchors.horizontalCenter: parent.horizontalCenter
		width: container.width
		y: 5
	}
	Item {
		id: groupGameListHeaders
		anchors {
			horizontalCenter: parent.horizontalCenter
			top: groupLists.bottom
			topMargin: 30
		}
		width: groupGameListView.width
		height: childrenRect.height + 4
		Item {
			anchors {
				left: parent.left
				right: parent.horizontalCenter
				rightMargin: 5;
			}
			height: childrenRect.height
			Text {
				anchors.horizontalCenter: parent.horizontalCenter
				text: "Stairs end"
				font.bold: true
			}
		}
		Item {
			anchors {
				left: parent.horizontalCenter
				leftMargin: 5;
				right: parent.right
			}
			height: childrenRect.height
			Text {
				anchors.horizontalCenter: parent.horizontalCenter
				text: "Clock end"
				font.bold: true
			}
		}
	}
	GroupGameView {
		id: groupGameListView
		anchors {
			horizontalCenter: parent.horizontalCenter
			top: groupGameListHeaders.bottom
			topMargin: 5
			bottom: parent.bottom
			bottomMargin: 10
		}
		model: Logic.getGroupStageGames()
		focus: true
	}
}

// vim:ft=qml
