import QtQuick 1.0
import "logic.js" as Logic

FocusScope {
	id: finalPanel
	Column {
		anchors {
			top: parent.top
			topMargin: 5
			left: parent.left
			right: parent.right
		}
		spacing: 5
		Text {
			id: playoffTitle
			anchors.horizontalCenter: parent.horizontalCenter
			text: "3rd/4th Place Playoff"
			font.bold: true
			font.pixelSize: 20
		}
		GamePanel {
			id: playoffGamePanel
			anchors.horizontalCenter: parent.horizontalCenter
			width: 400
			game: Logic.playoff_game
			playButtonEnabled: true
			onPlayClicked: {
				Logic.startFullIceGame(game);
			}
			onStopClicked: {
				Logic.endCurrentGames();
			}
		}
		Item {
			// spacer
			width: 400
			height: 50
		}
		Text {
			id: finalTitle
			anchors.horizontalCenter: parent.horizontalCenter
			text: "Final"
			font.bold: true
			font.pixelSize: 20
		}
		GamePanel {
			id: finalGamePanel
			anchors.horizontalCenter: parent.horizontalCenter
			width: 400
			game: Logic.final_game
			playButtonEnabled: true
			onPlayClicked: {
				Logic.startFullIceGame(game);
			}
			onStopClicked: {
				Logic.endCurrentGames();
			}
		}
	}
}

// vim:ft=qml
