import QtQuick 1.0
import "logic.js" as Logic

Flow {
	id: container
	//height: childrenRect.height
	spacing: 5
	Repeater {
		model: Logic.getGroupCount()
		Item {
			height: groupListContainer.height
			width: groupListContainer.width
			Rectangle {
				anchors.fill: parent
				color: Logic.groupColor(index)
				opacity: 0.75
				radius: 5
			}
			Item {
				id: groupListContainer
				height: childrenRect.height + 10
				width: groupView.width + 10
				Text {
					id: groupTitle
					text: "Group " + (index + 1)
					font.bold: true
					y: 5
					anchors.horizontalCenter: parent.horizontalCenter
				}
				GroupView {
					id: groupView
					interactive: false
					x: 5; y: 5 + groupTitle.height + 5
					model: Logic.getGroups(index)
					Component.onCompleted: {
						for (var i = 0; i < Logic.getGroups(index).length; ++i) {
							var oldOnChanged = Logic.getGroups(index)[i].onChanged;
							Logic.getGroups(index)[i].onChanged = function() {
								oldOnChanged();
								if (typeof(model) != 'undefined') {
									Logic.sortGroup(index);
									model = Logic.getGroups(index)
								}
							}
						}
					}
				}
			}
		}
	}
}

// vim:ft=qml
