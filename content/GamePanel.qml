import QtQuick 1.0
import "logic.js" as Logic

Item {
	id: container
	property variant game: null
	property bool isGroupGame: false
	property alias backgroundOpacity: backRect.opacity
	property color backgroundColorNormal: Logic.gameColor(game, isGroupGame, false)
	property color backgroundColorPlaying: Logic.gameColor(game, isGroupGame, true)
	property bool editButtonEnabled: true
	property bool playButtonEnabled: false


	signal playClicked
	signal stopClicked

	Rectangle {
		id: backRect
		anchors.fill: parent
		color: (Logic.realGameObject(game).isCurrentlyPlaying() ? container.backgroundColorPlaying : container.backgroundColorNormal)
		opacity: 0.75
		radius: 5
		Image {
			id: leftPlayMarker
			anchors {
				left: parent.left
				leftMargin: 5
				top: parent.top
				topMargin: 5
				bottom: parent.bottom
				bottomMargin: 5
			}
			width: height * 2 / 3
			sourceSize.width: width
			sourceSize.height: height
			source: "images/right-arrow.svg"
			opacity: Logic.realGameObject(game).isCurrentlyPlaying() ? 1 : 0
			Behavior on opacity { NumberAnimation { duration: 150 } }
		}
		Image {
			id: rightPlayMarker
			anchors {
				right: parent.right
				rightMargin: 5
				top: parent.top
				topMargin: 5
				bottom: parent.bottom
				bottomMargin: 5
			}
			width: height * 2 / 3
			sourceSize.width: width
			sourceSize.height: height
			source: "images/left-arrow.svg"
			opacity: Logic.realGameObject(game).isCurrentlyPlaying() ? 1 : 0
			Behavior on opacity { NumberAnimation { duration: 150 } }
}
		Behavior on color { ColorAnimation { duration: 150 } }
	}
	Component.onCompleted: {
		var oldOnChanged = Logic.realGameObject(game).onChanged;
		Logic.realGameObject(game).onChanged = function() {
			oldOnChanged();
			if (typeof(game) != 'undefined') {
				game = this;
			}
		}
	}
	height: col.height
	width: Math.max(team1Edit.width, team2Edit.width)
	MouseArea {
		id: mouseArea
		anchors.fill: parent
		hoverEnabled: true
	}
	Column {
		id: col
		height: childrenRect.height
		anchors.left: parent.left
		anchors.right: parent.right
		TeamNameScoreEditable {
			id: team1Edit
			anchors.horizontalCenter: parent.horizontalCenter
			teamName: game.team1.name
			teamScore: (Logic.realGameObject(game).team1Score == undefined) ? 0 : Logic.realGameObject(game).team1Score
			isWinner: Logic.realGameObject(game).winner() == Logic.realGameObject(game).team1
			showScore: (Logic.realGameObject(game).team1Score != undefined)
		}
		Text {
			anchors.horizontalCenter: parent.horizontalCenter
			text: "v"
		}
		TeamNameScoreEditable {
			id: team2Edit
			anchors.horizontalCenter: parent.horizontalCenter
			teamName: game.team2.name
			teamScore: (Logic.realGameObject(game).team2Score == undefined) ? 0 : Logic.realGameObject(game).team2Score
			isWinner: Logic.realGameObject(game).winner() == Logic.realGameObject(game).team2
			showScore: (Logic.realGameObject(game).team2Score != undefined)
		}
	}
	Item {
		id: buttonBlock
		anchors {
			top: parent.top
			topMargin: 2
			right: parent.right
			rightMargin: Logic.realGameObject(game).isCurrentlyPlaying() ? (2+rightPlayMarker.width+rightPlayMarker.anchors.rightMargin) : 2
			bottom: parent.bottom
			bottomMargin: 2
		}
		Behavior on anchors.rightMargin { NumberAnimation { duration: 150 } }
		width: 15
		opacity: (mouseArea.containsMouse || normalModeButtons.containsMouse || editModeButtons.visible) ? 1 : 0
		Behavior on opacity { NumberAnimation {} }
		Column {
			id: normalModeButtons
			anchors.top: parent.top
			anchors.right: parent.right
			spacing: 2
			property bool containsMouse: editButton.containsMouse || playButton.containsMouse
			ImageButton {
				id: editButton
				source: "images/edit.svg"
				width: buttonBlock.width; height: buttonBlock.width
				sourceSize.width: buttonBlock.width
				sourceSize.height: buttonBlock.width
				visible: container.editButtonEnabled
				onClicked: {
					container.state = 'editing';
				}
			}
			ImageButton {
				id: playButton
				source: Logic.realGameObject(game).isCurrentlyPlaying() ? "images/stop.svg" : "images/play.svg"
				width: buttonBlock.width; height: buttonBlock.width
				sourceSize.width: buttonBlock.width
				sourceSize.height: buttonBlock.width
				visible: container.playButtonEnabled
				onClicked: {
					if (Logic.realGameObject(game).isCurrentlyPlaying())
						container.stopClicked()
					else
						container.playClicked()
				}
			}
		}
		Column {
			id: editModeButtons
			visible: false
			anchors.top: parent.top
			anchors.right: parent.right
			spacing: 2
			ImageButton {
				id: cancelButton
				source: "images/cancel.svg"
				width: buttonBlock.width; height: buttonBlock.width
				sourceSize.width: buttonBlock.width
				sourceSize.height: buttonBlock.width
				onClicked: {
					team1Edit.teamScore = (Logic.realGameObject(game).team1Score == undefined) ? 0 : Logic.realGameObject(game).team1Score
					team2Edit.teamScore = (Logic.realGameObject(game).team2Score == undefined) ? 0 : Logic.realGameObject(game).team2Score
					container.state = ''
				}
			}
			ImageButton {
				id: okButton
				source: "images/ok.svg"
				width: buttonBlock.width; height: buttonBlock.width
				sourceSize.width: buttonBlock.width
				sourceSize.height: buttonBlock.width
				onClicked: {
					var team1Score = team1Edit.teamScore;
					var team2Score = team2Edit.teamScore;
					Logic.realGameObject(game).setScore(team1Score, team2Score);
					container.state = ''
				}
			}
		}
	}

	states: [
		State {
			name: "editing"
			PropertyChanges { target: editModeButtons; visible: true }
			PropertyChanges { target: normalModeButtons; visible: false }
			PropertyChanges { target: team1Edit; inEditMode: true }
			PropertyChanges { target: team2Edit; inEditMode: true }
		}
	]
}

// vim:ft=qml
