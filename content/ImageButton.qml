import QtQuick 1.0

Image {
	id: container

	property alias containsMouse: mouseArea.containsMouse
	signal clicked

	smooth: true

	MouseArea {
		id: mouseArea
		anchors.fill: parent
		hoverEnabled: true
		onClicked: { container.clicked() }
		enabled: container.visible && container.opacity > 0
	}
	opacity: mouseArea.containsMouse ? 1 : 0.5;
	Behavior on opacity {
		NumberAnimation {}
	}
}

// vim:ft=qml
