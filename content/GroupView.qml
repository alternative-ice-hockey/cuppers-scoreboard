import QtQuick 1.0
import "logic.js" as Logic

ListView {
	width: 240; height: model.length * 20 + 20
	delegate: Item {
		width: parent.width; height: 20
		Text {
			text: Logic.limitText(model.modelData.name, 28)
			anchors.top: parent.top
			anchors.left: parent.left
		}
		Row {
			Text {
				text: model.modelData.points
				horizontalAlignment: Text.AlignHCenter
				width: 20
			}
			Text {
				text: model.modelData.goalDifference
				horizontalAlignment: Text.AlignHCenter
				width: 20
			}
			spacing: 5
			anchors.top: parent.top
			anchors.right: parent.right
		}
	}
	header: Item {
		width: parent.width; height: 20
		Text {
			text: "Team"
			anchors.top: parent.top
			anchors.left: parent.left
			font.bold: true
		}
		Row {
			Text {
				text: "P"
				horizontalAlignment: Text.AlignHCenter
				font.bold: true
				width: 20
			}
			Text {
				text: "GD"
				horizontalAlignment: Text.AlignHCenter
				font.bold: true
				width: 20
			}
			spacing: 5
			anchors.top: parent.top
			anchors.right: parent.right
		}
	}
}

// vim:ft=qml
