import QtQuick 1.0
import "logic.js" as Logic

ListView {
	width: 400
	clip: true
	delegate: Item {
		width: parent.width; height: childrenRect.height + 4
		GamePanel {
			y: 2
			anchors {
				left: parent.left
				right: parent.right
			}
			game: model.modelData
			playButtonEnabled: true
			onPlayClicked: {
				Logic.startFullIceGame(model.modelData);
			}
			onStopClicked: {
				Logic.endCurrentGames();
			}
		}
	}
}

// vim:ft=qml
