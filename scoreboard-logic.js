var teams = {};
function Team(name)
{
	this.name = name;
	this.points = 0;
	this.goalDifference = 0;
	this.onChanged = function() {};
	this.addWin = function(goalDifference) {
		this.points += 3;
		this.goalDifference += goalDifference;
		this.onChanged.call(this);
	};
	this.addDraw = function(goalDifference) {
		this.points += 1;
		this.goalDifference += goalDifference;
		this.onChanged.call(this);
	};
	this.addLose = function(goalDifference) {
		this.goalDifference += goalDifference;
		this.onChanged.call(this);
	};
	this.removeWin = function(goalDifference) {
		this.points -= 3;
		this.goalDifference -= goalDifference;
		this.onChanged.call(this);
	};
	this.removeDraw = function(goalDifference) {
		this.points -= 1;
		this.goalDifference -= goalDifference;
		this.onChanged.call(this);
	};
	this.removeLose = function(goalDifference) {
		this.goalDifference -= goalDifference;
		this.onChanged.call(this);
	};
	teams[name] = this;
}
function realTeamObject(team) {
	return teams[team.name];
}

// group sizes should differ by at most one
var groups = [];
function setupGroups() {
	groups[0] = [
		new Team("DESI BOYZ"),
		new Team("Got Wood"),
		new Team("LMH"),
		new Team("Wadham B"),
		new Team("Pembroke Pigs"),
		new Team("Ice Bears")
	];
	groups[1] = [
		new Team("Oriel Lions"),
		new Team("Snow Pun Intended"),
		new Team("Harris Manchester Manatees"),
		new Team("Who Am I?"),
		new Team("Smooth Criminals"),
		new Team("Trinity 2")
	];
	groups[2] = [
		new Team("Team 1"),
		new Team("Mother Puckers"),
		new Team("Big Horney Dave"),
		new Team("St Anne's Mighty Beavers"),
		new Team("The Ice Ducks"),
		new Team("Chairman Meow's Skating Cat-astrophe")
	];
	groups[3] = [
		new Team("Keble Komets"),
		new Team("Wadham A"),
		new Team("St Anne's Special Ducks"),
		new Team("Quack!"),
		new Team("HMS Worcester")
	];
	groups[4] = [
		new Team("Bladerunners"),
		new Team("Balliol Freshers"),
		new Team("Keble 2nd Team"),
		new Team("Magdwhat?"),
		new Team("Mighty Newts")
	];
	// arrange groups by size
	groups.sort(function (a,b) { return b.length - a.length; });
	for (var i = 0; i < groups.length; ++i) {
		for (var j = 0; j < groups[i].length; ++j) {
			groups[i][j].group = i;
		}
	}
}

function compareTeamsByPoints(team1, team2)
{
	if (team1.points == team2.points) {
		return team2.goalDifference - team1.goalDifference;
	}
	return team2.points - team1.points;
}

var groupsAreSorted = false;
function sortGroups()
{
	if (groupsAreSorted)
		return;
	for (var i = 0; i < groups.length; ++i) {
		groups[i].sort(compareTeamsByPoints);
	}
	groupsAreSorted = true;
}
function sortGroup(i)
{
	groups[i].sort(compareTeamsByPoints);
}

function getGroups(i)
{
	if (groups.length == 0)
		setupGroups();
	return groups[i];
}

function getGroupCount()
{
	if (groups.length == 0)
		setupGroups();
	return groups.length;
}

var current_game = null;

var all_games = [];
function Game(team1, team2)
{
	this.team1 = team1;
	this.team2 = team2;
	this.finished = false;
	this.onChanged = function() {}
	this.setScore = function(team1Score, team2Score) {
		if (team1Score == undefined || team2Score == undefined) {
			this.team1Score = undefined;
			this.team2Score = undefined;
			this.onChanged.call(this);
			return;
		}
		this.team1Score = team1Score;
		this.team2Score = team2Score;
		this.onChanged.call(this);
	}
	this.isCurrentlyPlaying = function() {
		if (current_game != null && current_game != undefined) {
			if ('team1' in current_game) {
				return current_game == this;
			} else {
				return current_game[0] == this || current_game[1] == this;
			}
		}
		return false;
	}
	this.isDraw = function() {
		return (this.team1Score != undefined) && (this.team1Score == this.team2Score);
	}
	this.winner = function() {
		if (this.team1Score == this.team2Score) {
			return undefined;
		} else if (this.team1Score > this.team2Score) {
			return team1;
		} else if (this.team1Score < this.team2Score) {
			return team2;
		}
	}
	this.loser = function() {
		if (this.team1Score == this.team2Score) {
			return undefined;
		} else if (this.team1Score > this.team2Score) {
			return team2;
		} else if (this.team1Score < this.team2Score) {
			return team1;
		}
	}
	this.id = all_games.length;
	all_games[this.id] = this;
}
function GroupStageGame(team1, team2)
{
	var that = new Game(team1, team2);
	that.setScore = function(team1Score, team2Score) {
		if (this.team1Score != undefined && this.team2Score != undefined) {
			if (this.team1Score > this.team2Score) {
				this.team1.removeWin(this.team1Score - this.team2Score);
				this.team2.removeLose(this.team2Score - this.team1Score);
			} else if (this.team1Score < this.team2Score) {
				this.team1.removeLose(this.team1Score - this.team2Score);
				this.team2.removeWin(this.team2Score - this.team1Score);
			} else {
				this.team1.removeDraw(this.team1Score - this.team2Score);
				this.team2.removeDraw(this.team2Score - this.team1Score);
			}
		}
		if (team1Score == undefined || team2Score == undefined) {
			this.team1Score = undefined;
			this.team2Score = undefined;
			this.onChanged.call(this);
			return;
		}
		this.team1Score = team1Score;
		this.team2Score = team2Score;
		if (this.team1Score > this.team2Score) {
			this.team1.addWin(this.team1Score - this.team2Score);
			this.team2.addLose(this.team2Score - this.team1Score);
		} else if (this.team1Score < this.team2Score) {
			this.team1.addLose(this.team1Score - this.team2Score);
			this.team2.addWin(this.team2Score - this.team1Score);
		} else {
			this.team1.addDraw(this.team1Score - this.team2Score);
			this.team2.addDraw(this.team2Score - this.team1Score);
		}
		this.onChanged.call(this);
	}
	return that;
}
function realGameObject(game) {
	if (typeof(game) != 'object')
		return undefined;
	return all_games[game.id];
}
function endCurrentGames()
{
	var old_current_game = current_game;
	current_game = null;
	if (old_current_game != null && old_current_game != undefined) {
		if ('team1' in old_current_game) {
			old_current_game.finished = true;
			old_current_game.onChanged.call(old_current_game);
		} else {
			old_current_game[0].finished = true;
			old_current_game[1].finished = true;
			old_current_game[0].onChanged.call(old_current_game[0]);
			old_current_game[1].onChanged.call(old_current_game[1]);
		}
	}
}
function startFullIceGame(game)
{
	endCurrentGames();
	current_game = realGameObject(game);
	current_game.setScore(undefined, undefined);
	current_game.finished = false;
	current_game.onChanged.call(current_game);
}
function startHalfIceGames(game1, game2)
{
	endCurrentGames();
	current_game = [realGameObject(game1),realGameObject(game2)];
	current_game[0].setScore(undefined, undefined);
	current_game[0].finished = false;
	current_game[0].onChanged.call(current_game[0]);
	if (typeof(current_game[1]) == 'object') {
		current_game[1].setScore(undefined, undefined);
		current_game[1].finished = false;
		current_game[1].onChanged.call(current_game[1]);
	}
}

// Group stages are half ice (two games at once)
var group_stage_games = [];
var group_stage_playoffs = [];
// Tournament stages are full ice (one game at once)
var quarter_final_games = [];
var semi_final_games = [];
var playoff_game = null;
var final_game = null;

Array.prototype.map = function(f) {
	var result = [];
	for (var i = 0; i < this.length; ++i) {
		result[i] = f(this[i]);
	}
	return result;
};

Array.prototype.removeMatching = function(f) {
	var removed = [];
	for (var i = 0; i < this.length; ++i) {
		if (f(this[i])) {
			removed.push(this[i]);
			this.splice(i, 1);
		}
	}
	return removed;
};

Array.prototype.containsMatch = function(f) {
	for (var i = 0; i < this.length; ++i) {
		if (f(this[i])) {
			return true;
		}
	}
	return false;
};

function generateGameList(team_count)
{
	// the list of teams.  Team numbers start at 0
	var team_list = [];
	for (var i = 0; i < team_count; ++i) {
		team_list[i] = i;
	}
	// Every time we insert a game into the running order, we
	// remove the first occurrences of each team from this pool.
	// This is to make sure that, towards the end of the
	// selection process, we don't pick a teamA that has no more
	// games to play.  An alternative approach would be to keep
	// track of how many games each team has played.
	var team_pool = [];
	for (var i = 0; i < team_count - 1; ++i) {
		team_pool = team_pool.concat(team_list);
	}

	// a list of [teamA, teamB] pairs (two-element arrays)
	var game_list = [];
	// checks whether a game has already been inserted into the running order
	var hadGame = function(teamA, teamB) {
		for (var i = 0; i < game_list.length; ++i) {
			if ((teamA == game_list[i][0] &&
					teamB == game_list[i][1]) ||
					(teamA == game_list[i][1] &&
					 teamB == game_list[i][0])) {
				return true;
			}
		}
		return false;
	};

	// we keep track of how many games ago a team last played, and try to
	// select teams that haven't played for a while when inserting a game
	var lastSeen = [];
	for (var i = 0; i < team_count - 1; ++i) {
		lastSeen.push(0);
	}

	while (team_pool.length > 0) {
		// put teams that have played more recently at the end
		team_list.sort(function (t1, t2) {
			return lastSeen[t2] - lastSeen[t1];
		});

		// Select the first team for the game, aiming to choose one
		// that hasn't played for a while.
		var aPos = -1;
		// No point looking too far ahead in team_pool:
		var searchAhead = Math.min(team_pool.length, team_count);
		for (var i = 0; aPos >= 0 && i < team_list.length; ++i) {
			for (var j = 0; aPos >= 0 && j < searchAhead; ++j) {
				if (team_list[i] = team_pool[j])
					aPos = j;
			}
		}
		if (aPos < 0)
			aPos = 0;
		var teamA = team_pool[aPos];
		team_pool.splice(aPos, 1); // remove element aPos

		// Find a team to play against teamA, prioritising teams that
		// haven't played for a while.
		var teamBPool = [];
		for (var i = 0; teamBPool.length < team_count - 1 && i < team_pool.length; ++i) {
			if (team_pool[i] != teamA) {
				if (!teamBPool.containsMatch(function (t) {team_pool[i] == t.team})) {
					teamBPool.push({
						team:team_pool[i],
						lastSeen:lastSeen[team_pool[i]],
						firstIndex:i
					});
				}
			}
		}
		teamBPool.sort(function (t1,t2) { return t2.lastSeen - t1.lastSeen; });
		while (hadGame(teamA, teamBPool[0].team)) {
			teamBPool.shift();
		}
		var teamB = teamBPool[0].team;
		team_pool.splice(teamBPool[0].firstIndex, 1); // remove element i

		var game = [teamA, teamB];
		game_list.push(game);

		for (var i = 0; i < team_count - 1; ++i) {
			if (i == teamA || i == teamB)
				lastSeen[i] = 0;
			else
				lastSeen[i] += 1;
		}

	}
	// HACK: 6 teams has the same team playing the second-to-last and
	//       third-to-last games
	if (team_count == 6) {
		var tmp = game_list[game_list.length-2];
		game_list[game_list.length-2] = game_list[game_list.length-1];
		game_list[game_list.length-1] = tmp;
	}
	return game_list;
}

function generateGroupStageGamesForGroup(num)
{
	return generateGameList(groups[num].length).map(function (g) {
		return new GroupStageGame(
			groups[num][g[0]],
			groups[num][g[1]]
		);
	});
}

function generateGroupStageGames()
{
	var games = [];
	var firstShortGroup = 0;
	for (var i = 0; i < groups.length; ++i) {
		games[i] = generateGroupStageGamesForGroup(i);
		if (games[i].length == games[0].length)
			++firstShortGroup;
	}
	var allGames = [];
	function distributeArr(arr)
	{
		var result = [];
		var resultLength = arr.length * arr[0].length;
		for (var i = 0; i < resultLength; ++i) {
			result.push(arr[i%arr.length].shift());
		}
		return result;
	}
	function distribute()
	{
		return distributeArr(arguments);
	}
	if (firstShortGroup >= games.length) {
		allGames = distributeArr(games);
	} else {
		var initialList = distributeArr(games.slice(0, firstShortGroup));
		var insertList = distributeArr(games.slice(firstShortGroup, games.length));

		var spacing30 = Math.round(((initialList.length + insertList.length) / insertList.length) * 30);
		var baseSpacing = Math.floor(spacing30 / 30);
		var wobbleNum = spacing30 % 30;
		var wobbleDenom = 30;
		if (wobbleNum) {
			for (var i = 2; i <= 5; ++i) {
				if ((wobbleNum % i == 0) && (wobbleDenom % i) == 0) {
					wobbleNum /= i;
					wobbleDenom /= i;
				}
			}
		}

		var i = 1;
		var j = 0;
		while (initialList.length || insertList.length) {
			i = (i + 1) % baseSpacing;
			if (!initialList.length || (insertList.length && (i == 0))) {
				allGames.push(insertList.shift());
				if (wobbleNum) {
					j = (j + 1) % wobbleDenom;
					if (j == wobbleNum)
						++baseSpacing;
					if (j == 0)
						--baseSpacing;
				}
			} else {
				allGames.push(initialList.shift());
			}
		}
	}
	while (allGames.length > 0) {
		var game1 = allGames.shift();
		var game2 = allGames.length ? allGames.shift() : null;
		group_stage_games.push([game1, game2]);
	}
}

function getGroupStageGames()
{
	if (groups.length == 0)
		setupGroups();
	if (group_stage_games.length == 0)
		generateGroupStageGames();
	return group_stage_games;
}

function hasGroupStagePlayoffs()
{
	if (groups.length == 0)
		setupGroups();
	return groups.length != 4;
}

// groups must be sorted
function findTopSecondTeam()
{
	sortGroups();
	var maxGroupSize = groups[0].length;
	var secondsPool = [];
	for (var i = 0; i < groups.length; ++i) {
		if (groups[i].length < maxGroupSize)
			break;
		secondsPool.push(groups[i][1]);
	}
	secondsPool.sort(compareTeamsByPoints);
	return secondsPool[0];
}

function generateGroupStagePlayoffGames()
{
	function setGSPGame(i, team1, team2) {
		if (group_stage_playoffs.length > i) {
			var g = group_stage_playoffs[i];
			if (g.team1 == team1 && g.team2 == team2) {
				return;
			}
		}
		group_stage_playoffs[i] = new Game(team1, team2);
	}

	sortGroups();

	if (groups.length != 5) {
		return false;
	}

	var ignoreGroup = findTopSecondTeam().group;
	var teamList = [];
	for (var i = 0; i < groups.length; ++i) {
		if (i != ignoreGroup) {
			teamList.push(groups[i][1]);
		}
	}
	setGSPGame(0, teamList[0], teamList[1]);
	setGSPGame(1, teamList[2], teamList[3]);
	return true;
}

function generateQuarterFinalGames()
{
	function setQFGame(i, team1, team2) {
		if (quarter_final_games.length > i) {
			var g = quarter_final_games[i];
			if (g.team1 == team1 && g.team2 == team2) {
				return;
			}
		}
		quarter_final_games[i] = new Game(team1, team2);
	}

	if (hasGroupStagePlayoffs()) {
		for (var i = 0; i < group_stage_playoffs.length; ++i) {
			if (group_stage_playoffs[i].winner() == undefined) {
				return false;
			}
		}
	}

	sortGroups();
	if (groups.length == 4) {
		setQFGame(0, groups[0][0], groups[1][1]);
		setQFGame(1, groups[2][0], groups[3][1]);
		setQFGame(2, groups[0][1], groups[1][0]);
		setQFGame(3, groups[2][1], groups[3][0]);
	} else if (group_stage_playoffs.length == 2) {
		setQFGame(0, groups[0][0], groups[1][0]);
		setQFGame(1, groups[2][0], findTopSecondTeam());
		setQFGame(2, groups[3][0], group_stage_playoffs[0].winner());
		setQFGame(3, groups[4][0], group_stage_playoffs[1].winner());
	}
	return true;
}

function generateSemiFinalGames()
{
	function setSFGame(i, team1, team2) {
		if (semi_final_games.length > i) {
			var g = semi_final_games[i];
			if (g.team1 == team1 && g.team2 == team2) {
				return;
			}
		}
		semi_final_games[i] = new Game(team1, team2);
	}

	for (var i = 0; i < quarter_final_games.length; ++i) {
		if (quarter_final_games[i].winner() == undefined) {
			return false;
		}
	}
	setSFGame(0, quarter_final_games[0].winner(), quarter_final_games[1].winner());
	setSFGame(1, quarter_final_games[2].winner(), quarter_final_games[3].winner());
	return true;
}

function generateFinalGames()
{
	function getGame(oldValue, team1, team2) {
		if (typeof(oldValue) == 'object' && oldValue != null) {
			if (oldValue.team1 == team1 && oldValue.team2 == team2) {
				return oldValue;
			}
		}
		return new Game(team1, team2);
	}
	for (var i = 0; i < semi_final_games.length; ++i) {
		if (semi_final_games[i].winner() == undefined) {
			return false;
		}
	}
	final_game = getGame(final_game, semi_final_games[0].winner(), semi_final_games[1].winner());
	playoff_game = getGame(playoff_game, semi_final_games[0].loser(), semi_final_games[1].loser());
	return true;
}

